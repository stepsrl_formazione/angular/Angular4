import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MARVELAPIService } from './services/marvelapi.service';
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';


import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule, // validation
    HttpModule,
    NgbModule.forRoot()
  ],
  providers: [MARVELAPIService],
  bootstrap: [AppComponent]
})
export class AppModule { }
