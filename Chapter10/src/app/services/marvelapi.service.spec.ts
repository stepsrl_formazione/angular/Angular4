import { TestBed, inject } from '@angular/core/testing';

import { MARVELAPIService } from './marvelapi.service';

describe('MARVELAPIService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MARVELAPIService]
    });
  });

  it('should ...', inject([MARVELAPIService], (service: MARVELAPIService) => {
    expect(service).toBeTruthy();
  }));
});
