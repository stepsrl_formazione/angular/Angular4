export interface Observer{
    notify(value?:any, subject?:Subject);
}

export class HumanObserver implements Observer{

    constructor(private name:string){}

    notify(value?:any, subject?:Subject){
        console.log(this.name, 'received', value, 'from', subject);
    }
}

export class Subject{

    private observers:Observer[] = [];

    attachObserver(observer:Observer):void{
        this.observers.push(observer);
    }

    detachObserver(observer:Observer):void{

        let index:number = this.observers.indexOf(observer);
        if(index > -1){
            this.observers.splice(index, 1);
        }else{
            throw "Unknown observer";
        }
    }

    protected notifyObservers(value?:any){

        for (var i = 0; i < this.observers.length; ++i) {
            this.observers[i].notify(value, this);
        }
    }
}

export class IMDB extends Subject{

    private movies:string[] = [];

    public addMovie(movie:string){
        this.movies.push(movie);
        this.notifyObservers(movie);
    }

}

let imbd:IMDB = new IMDB();
let enrico:HumanObserver = new HumanObserver("Enrico Ghezzi");

imbd.attachObserver(enrico);
imbd.addMovie("Blob");
imbd.detachObserver(enrico);
imbd.addMovie("Die Hard");