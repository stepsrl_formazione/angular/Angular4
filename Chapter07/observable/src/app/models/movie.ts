export class Movie {
public constructor(
private _id:number,
private _title: string,
private _phase: string,
private _release_year: number,
private _running_time: number,
private _release_date: string,
private _budget: number,
private _gross: number,
private _time_stamp:Date){
}


public toString = () : string => {
return `Movie (id: ${this._id},
title: ${this._title},
phase: ${this._phase},
release_year: ${this._release_year},
running_time: ${this._running_time},
release_date: ${this._release_date},
budget: ${this._budget},
gross: ${this._gross},
time_stamp: ${this._time_stamp})`;
}

    get id(): number{
        return this._id;
    }
    get title(): string{
        return this._title;
    }
    get phase(): string{
        return this._phase;
    }
    get release_year(): number{
        return this._release_year;
    }
    get running_time(): number{
        return this._running_time;
    }
    get release_date(): string{
        return this._release_date;
    }
    get budget(): number{
        return this._budget;
    }
    get gross(): number{
        return this._gross;
    }
    get time_stamp(): Date{
        return this._time_stamp;
    }

    set id(id: number){
        this._id = id;
    }
    set title(title: string){
        this._title = title;
    }
    set phase(phase: string){
        this._phase = phase;
    }
    set release_year(release_year: number){
        this._release_year = release_year;
    }
    set running_time(running_time: number){
        this._running_time = running_time;
    }
    set release_date(release_date: string){
        this._release_date = release_date;
    }
    set budget(budget: number){
        this._budget = budget;
    }
    set gross(gross: number){
        this._gross = gross;
    }
    set time_stamp(time_stamp:Date){
        this._time_stamp = time_stamp;
    }

}

export enum MovieFields{
    id,
    title,
    phase,
    release_year,
    running_time,
    release_date,
    budget,
    gross,
    time_stamp
}
