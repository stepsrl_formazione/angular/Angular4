import { Component } from '@angular/core';
import { Movie, MovieFields } from './models/movie'
import { MARVELAPIService} from './services/marvelapi.service'


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  	title = 'app';
  	private movies:Movie[] = [];
  	private moviesField:Movie[] = [];
	private error:boolean = false;
	private finished:boolean = false;
	constructor(private MARVELAPI:MARVELAPIService){
		this.MARVELAPI.fetchOneById(1).subscribe(
			value => {this.movies.push(value); console.log("Component",value)},
			error => this.error = true,
			() => this.finished = true
		);

		let MARVELSubscription = this.MARVELAPI.fetchByField(MovieFields.release_year, 2015).subscribe(
			value => { 	this.moviesField.push(value); console.log("Component", value); 
			if(this.moviesField.length>1){ MARVELSubscription.unsubscribe(); } // non effettuo più operazioni
		},
			error => this.error = true,
			() => this.finished = true
		)
	}
}
