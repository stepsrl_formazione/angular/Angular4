import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Movie, MovieFields } from '../models/movie';
import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';

@Injectable()
export class MARVELAPIService {

  private moviesUrl: string = "http://localhost:3000/movies";
  //private moviesPath: string = "assets/marvel-cinematic-universe.json";

    constructor(private http: Http) { }

     /**
     * Return an Observable to a Movie matching id
     * @param  {number}            id
     * @return {Observable<Movie>}   
     */
   	 public fetchOneById(id: number): Observable<Movie> {
        console.log('fecthOneById', id);

        return this.http.get(this.moviesUrl)
            /**
             * Transforms the result of the http get, which is observable
             * into one observable by item.
             */
            .flatMap(res => res.json())
            /**
             * Filters movies by their movie_id
             */
            .filter((movie: any) => {
                console.log("filter", movie);
                return (movie.id === id)
            })
            /**
             * Map the json movie item to the Movie model
             */
            .map((movie: any) => {
                console.log("map", movie);
                return new Movie(
                    movie.id,
                    movie.title,
                    movie.phase,
                    movie.release_year,
                    movie.running_time,
                    movie.release_date,
                    movie.budget,
                    movie.gross,
                    movie.time_stamp
                );
            });
    } 

    public fetchByField(field: MovieFields, value: any) {
        console.log('fetchByField', field, value);

        return this.http.get(this.moviesUrl)
            /**
             * Transforms the result of the http get, which is observable
             * into one observable by item.
             */
            .flatMap(res => res.json())
            /**
             * Filters movies by their field
             */
            .filter((movie: any) => {
                console.log("filter", movie);
                return (movie[MovieFields[field]] === value)
            })
            /**
             * Map the json movie item to the Movie model
             */
            .map((movie: any) => {
                console.log("map", movie);
                return new Movie(
                    movie.id,
                    movie.title,
                    movie.phase,
                    movie.release_year,
                    movie.running_time,
                    movie.release_date,
                    movie.budget,
                    movie.gross,
                    movie.time_stamp
                );
            });

    }

}
