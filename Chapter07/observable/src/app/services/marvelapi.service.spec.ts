import { TestBed, inject } from '@angular/core/testing';

import { MarvelapiService } from './marvelapi.service';

describe('MarvelapiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MarvelapiService]
    });
  });

  it('should be created', inject([MarvelapiService], (service: MarvelapiService) => {
    expect(service).toBeTruthy();
  }));
});
