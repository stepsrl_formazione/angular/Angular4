import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Movie, MovieFields } from '../models/movie';
import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';

@Injectable()
export class MARVELAPIService {

  private moviesUrl: string = "http://localhost:3000/movies";
  
    constructor(private http: Http) { }



  /**
   * Return a Promise to a Movie matching id
   * @param  {number}            id
   * @return {Promise<Movie>}   
   */
  public fetchOneById(id:number):Promise<Movie>{
      console.log('fetchOneById', id);

        return this.http.get(this.moviesUrl)
        /**
         * Transforms the result of the http get, which is observable
         * into one observable by item.
         */
        .flatMap(res => res.json())
        /**
         * Filters movies by their movie_id
         */
        .filter((movie:any)=>{
            console.log("filter", movie);
            return (movie.movie_id === id)
        })
        .toPromise()
        /**
         * Map the json movie item to the Movie model
        */
        .then((movie:any) => {
            console.log("map", movie); 
            return new Movie(
                movie.movie_id,
                movie.title,
                movie.phase,
                movie.release_year,
                movie.running_time,
                movie.release_date,
                movie.budget,
                movie.gross,
                movie.time_stamp
            )
        });
  } 

  /**
   * Private member storing pending promises
   */
  private promises:Promise<Movie[]>[] = [];

  /**
   * Register one promess for field/value. Returns this
   * for chaining i.e.
   *
   *  byField(Y, X)
   * .or(...)
   * .fetch()
   * 
   * @param  {MovieFields}    field
   * @param  {any}            value
   * @return {MARVELAPIService}      
   */
  public byField(field:MovieFields, value:any):MARVELAPIService{
      this.promises.push(this.fetchByField(field, value));
      return this;
  }

  /**
   * Convenient method to make the calls more readable, i.e.
   * 
   *  byField(Y, X)
   * .or(...)
   * .fetch()
   *
   * instead of 
   *
   *  byField(Y, X)
   * .byField(...)
   * .fetch()
   * 
   * @param  {MovieFields}    field
   * @param  {any}            value
   * @return {IMDBAPIService}      
   */
  public or(field:MovieFields, value:any):MARVELAPIService{
      return this.byField(field, value);
  }

  /**
   * Join all the promises and return the aggregated result.
   * 
   * @return {Promise<Movie[]>}
   */
  public fetch():Promise<Movie[]>{
      return Promise.all(this.promises).then((results:any) => {
            //result is am array of movie arrays. One array per 
            //promise. We need to flatten it.
            let resArray: Promise<Movie[]> = [].concat.apply([], results);
            
            return resArray;
            //return [].concat.apply([], results);

      });
  }

  public fetchByField(field:MovieFields, value:any):Promise<Movie[]>{
      console.log('fetchByField', field, value);

      return this.http.get(this.moviesUrl)
        /**
         * Transforms the result of the http get, which is observable
         * into one observable by item.
         */
        .map(res => res.json().filter(
            (movie)=>{
                return (movie[MovieFields[field]] === value)
            })
        )
        .toPromise()
        /**
         * Map the json movie item to the Movie model
        */
        .then((jsonMovies:any[]) => {

            console.log("map", jsonMovies); 
            let movies:Movie[] = [];

            for (let i = 0; i < jsonMovies.length; i++) {
                movies.push(
                    new Movie(
                        jsonMovies[i].movie_id,
                        jsonMovies[i].title,
                        jsonMovies[i].phase,
                        jsonMovies[i].release_year,
                        jsonMovies[i].running_time,
                        jsonMovies[i].release_date,
                        jsonMovies[i].budget,
                        jsonMovies[i].gross,
                        jsonMovies[i].time_stamp
                    )
                )
            }
            return movies;
        });

  }
}
