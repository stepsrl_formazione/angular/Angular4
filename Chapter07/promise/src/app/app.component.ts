import { Component } from '@angular/core';
import { Movie, MovieFields } from './models/movie'
import { MARVELAPIService} from './services/marvelapi.service'


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  	title = 'app';
  	private movies:Movie[] = [];
  	private moviesField:Movie[] = [];
	private error:boolean = false;
	private finished:boolean = false;
	constructor(private MARVELAPI:MARVELAPIService){
		this.MARVELAPI.fetchOneById(1).then(
			value => {
				this.movies.push(value);
				console.log("Component", value)
			},
			error => this.error = true
		);

		// simple fetch
		this.MARVELAPI.fetchByField(MovieFields.release_year, 2015)
			.then(
				value => {
					this.moviesField = value;
					console.log("Component", value)
				},
				error => this.error = true
		);

		this.MARVELAPI.byField(MovieFields.release_year, 2015)
			.or(MovieFields.release_year, 2014)
			.or(MovieFields.phase, "Phase Two")
			.fetch()
			.then(
				value => {
					/*let filtered = value.filter((elem, index, self) => self.findIndex(
    					(t:Movie) => {return (t.id === elem.id)}) === index)
					this.moviesField = filtered;
					*/
					this.moviesField = value;
					console.log("Component", value)
				},
				error => this.error = true
		);
	}


}
