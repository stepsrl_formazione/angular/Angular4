import { Component } from '@angular/core';
import { Routes, Router } from '@angular/router';

@Component({
    templateUrl: './products.component.html',
    styles: ['.container {background-color: #fff;}']
})
export class ProductsComponent {

	constructor(private router: Router) {  }

	navigateToServices(){
		this.router.navigate(['/services']);
	}

}
