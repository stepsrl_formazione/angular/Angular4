import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {  ActivatedRoute } from '@angular/router';

@Component({
  templateUrl: './products-details.component.html',
    styles: ['.container {background-color: #fff;}']
})

export class ProductsDetailsComponent implements OnInit {
    
    private selectedId: number;
    private sub;
    
  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
     this.sub = this.route.params.subscribe(params => {
    let id = params['id'];
    this.selectedId = id;
    console.log(id);
  });

}}