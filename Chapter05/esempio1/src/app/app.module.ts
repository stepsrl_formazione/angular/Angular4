import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { AboutComponent} from './about/about.component';
import { ServicesComponent} from './services/services.component';
import { ProductsComponent } from './products/products.component';

import { HashLocationStrategy, LocationStrategy } from "@angular/common";

const appRoutes: Routes = [
{ path: 'about', component: AboutComponent },
{ path: 'services', component: ServicesComponent },
{ path: 'products', redirectTo:'/new-products', pathMatch:'full'},
{ path: '**', component: AboutComponent }
];
@NgModule({
imports: [
BrowserModule,
FormsModule,
RouterModule.forRoot(appRoutes)
],
declarations: [
AppComponent,
AboutComponent,
ServicesComponent,
ProductsComponent,
],
bootstrap: [ AppComponent ],
providers: [{provide: LocationStrategy, useClass: HashLocationStrategy }]
})
export class AppModule { }