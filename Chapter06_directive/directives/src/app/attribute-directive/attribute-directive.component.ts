import { Component} from '@angular/core';

@Component({
 selector: 'my-app',
 styleUrls: ['./attribute-directive.component.css'],
 templateUrl:'./attribute-directive.component.html'
})
export class AttributeDirectiveComponent {
 title = 'Attribute Directive';
 errVal=false;
 sucVal=true;
 public username="Dave Gahan";
}
