import { Component } from '@angular/core';
import { User } from './shared/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Learning Angular 4';
  user = new User('Grohl', 10);

  constructor () {}

  changeDetectionDefault(): void {
		this.user.userName = 'My Name';
		this.user.userId = 10;
}
changeDetectionOnPush(): void {
this.user = new User('Mike', 10);
}
 
}
