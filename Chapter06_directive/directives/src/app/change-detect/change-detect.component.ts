import { Component, Input } from '@angular/core';
import { ChangeDetectionStrategy } from '@angular/core';
import { User } from '../shared/user';

@Component({
  selector: 'app-change-detect',
  templateUrl: './change-detect.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./change-detect.component.css']
})
export class ChangeDetectComponent {

  title = "Change Detection";
  @Input() user: User;

  constructor() { }

   changeDetectionDefault(): void {
		this.user.userName = 'My Name';
		this.user.userId = 10;
}
changeDetectionOnPush(): void {
this.user = new User('Mike', 10);
}
}


