class Customer {

constructor(private _name: string) {	
	}

	logCustomer() {		
		console.log('customer name is '+this._name );	
	}

	get name(){
		console.log("passo dal get");
		return this._name;
	}

	set name(newName: string){
		console.log("passo dal set");
		this._name=newName;
	}
}
		
var customer = new Customer("Mark Knopfler"); 
console.log(customer.name);
customer.name="Fausto";
customer.logCustomer();