var Customer = /** @class */ (function () {
    function Customer(_name) {
        this._name = _name;
    }
    Customer.prototype.logCustomer = function () {
        console.log('customer name is ' + this._name);
    };
    Object.defineProperty(Customer.prototype, "name", {
        get: function () {
            console.log("passo dal get");
            return this._name;
        },
        set: function (newName) {
            console.log("passo dal set");
            this._name = newName;
        },
        enumerable: true,
        configurable: true
    });
    return Customer;
}());
var customer = new Customer("Mark Knopfler");
console.log(customer.name);
customer.name = "Fausto";
customer.logCustomer();
