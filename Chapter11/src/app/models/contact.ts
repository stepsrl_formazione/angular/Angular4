export class Contact {

   

   public constructor(
        public id:number,
        public name: string,
        public phone: string,
        public address: string,
        public email: string,
        public website: string,
        public notes: string){

    }


    public toString = () : string => {

        return `Contact (id: ${this.id},
        name: ${this.name},
        phone: ${this.phone},
        address: ${this.address},
        email: ${this.email},
        website: ${this.website},
        notes: ${this.notes}`;
    }

   

}

export enum ContactFields{
    id,
    name,
    phone,
    address,
    email,
    website,
    notes
}