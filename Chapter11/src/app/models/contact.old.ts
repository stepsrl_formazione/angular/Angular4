export class Contact {

   

   public constructor(
        private _id:number,
        private _name: string,
        private _phone: string,
        private _address: string,
        private _email: string,
        private _website: string,
        private _notes: string){

    }


    public toString = () : string => {

        return `Contact (id: ${this._id},
        name: ${this._name},
        phone: ${this._phone},
        address: ${this._address},
        email: ${this._email},
        website: ${this._website},
        notes: ${this._notes}`;
    }

    get id(): number{
        return this._id;
    }
    get name(): string{
        return this._name;
    }
    get phone(): string{
        return this._phone;
    }
    get address(): string{
        return this._address;
    }
    get email(): string{
        return this._email;
    }
    get website(): string{
        return this._website;
    }
    get notes(): string{
        return this._notes;
    }
   

    set id(id: number){
        this._id = id;
    }
    set name(name: string){
        this._name = name;
    }
    set phone(phone: string){
        this._phone = phone;
    }
    set address(address: string){
        this._address = address;
    }
    set email(email: string){
        this._email = email;
    }
    set website(website: string){
        this._website = website;
    }
    set notes(notes: string){
        this._notes = notes;
    }
   

}

export enum ContactFields{
    id,
    name,
    phone,
    address,
    email,
    website,
    notes
}