import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { ContactsService } from '../services/contacts.service';
import { Contact } from '../models/contact';


@Injectable()
export class ContactResolve implements Resolve<Contact> {

  constructor(private contactsService: ContactsService) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.contactsService.find(+route.paramMap.get('id')); //unary operator + converts to number
  }
}