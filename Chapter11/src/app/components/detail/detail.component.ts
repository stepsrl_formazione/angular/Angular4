import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Contact } from '../../models/contact';
import { ContactsService } from '../../services/contacts.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit, OnChanges {
	
	contact: Contact;
  	constructor(private contactsService: ContactsService,
  				private route: ActivatedRoute
  				) { }

  	ngOnInit() {
  		let id = this.route.snapshot.paramMap.get('id');
  		this.contactsService.find(+id).subscribe( 
  			contact => this.contact = contact,
    		error => console.log(error),
    		() => console.log('chiamata terminata')
  		);
  		

  	}

  	ngOnChanges(){
  		console.log("ecco");
  	}

  	updateContact(event: any) {
  		this.contactsService.update(this.contact).subscribe(
  			contact => this.contact = contact,
    		error => console.log(error),
    		() => console.log('chiamata terminata')
  		
  		)
  	}


}
