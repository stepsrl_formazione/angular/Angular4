import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

import { Contact } from '../../models/contact';
import { ContactsService } from '../../services/contacts.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
	//property first
	private contactForm: FormGroup;
	private contact: Contact = new Contact(null,null,null,null,null,null,null); 
	private alertMessage: String = null;

  constructor(private contactsService: ContactsService, private formBuilder: FormBuilder) { 
  	this.contactForm = this.formBuilder.group({
		name: ['', Validators.required],
	    phone: ['', Validators.required],
	    address: ['', Validators.required],
	    email: ['', Validators.required],
	    website: '',
	    notes: ''
    });
  }

  ngOnInit() {
  }

  private submit(){
  	if(this.contactForm.valid){
  		console.log(this.contact);
  		this.contactsService.create(this.contact).subscribe(
  			(contact:any) => {console.log(contact); this.contactForm.reset();this.alertMessage="Contact created";},
  			error => console.log(error),
			() => console.log('chiamata terminata') 
  		);
	}
  	else {
  		console.log("error");
  	}

  }

}
