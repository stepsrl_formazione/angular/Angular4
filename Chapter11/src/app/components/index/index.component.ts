import { Component, OnInit } from '@angular/core';
import { ContactsService } from '../../services/contacts.service';
import { Contact } from '../../models/contact';
import {  ResponseWrapper } from '../../models/response-wrapper'
import { Observable } from 'rxjs/Rx';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  contacts: Contact[];
  alertMessage: String;

  constructor(
  	private contactsService: ContactsService
  	) { 
  	
  }

  ngOnInit() {
    this.queryAll();
  }

  delete(id: number) {

  	console.log(id);
    this.contactsService.delete(id).subscribe(
      value => {this.alertMessage="Contact deleted"; this.queryAll();},
    error => console.log(error),
    () => console.log('chiamata terminata')
      )

  }

  queryAll() {
      let obsRes: Observable<ResponseWrapper> = this.contactsService.query(); 
  obsRes.subscribe(
    value => this.contacts = value.json,
    error => console.log(error),
    () => console.log('chiamata terminata')
    )
  }

}
