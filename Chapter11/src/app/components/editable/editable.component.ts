import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'editable',
  templateUrl: './editable.component.html',
  styleUrls: ['./editable.component.css']
})
export class EditableComponent implements OnInit {
	
	@Input() field: string;
	@Input() value: string;
	@Output() valueChange: EventEmitter<string> = new EventEmitter<string>();

	editor: Editor = new Editor(false, this.value);;

  constructor() { }

  ngOnInit() {
  	this.field = (this.field) ? this.field : 'text';
  	this.editor = new Editor(false, this.value);
  }

  toggleEditor() {
  	this.editor.showing = !this.editor.showing;
  	this.editor.value = this.value;
  }

  save() {
  	this.value = this.editor.value;
  	this.valueChange.emit(this.value);
  	this.toggleEditor();
  }

}

class Editor {

	constructor(private _showing: boolean, private _value: any) {} 

	get showing(): boolean {
		return this._showing;
	}
	
	get value(): any {
		return this._value;
	}

	set showing(showing: boolean){
		this._showing = showing;
	}

	set value(value: any){
		this._value = value;
	}

}