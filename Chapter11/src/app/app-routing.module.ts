import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IndexComponent } from './components/index/index.component';
import { AddComponent } from './components/add/add.component';
import { DetailComponent } from './components/detail/detail.component';

const routes: Routes = [
	{ path: '', component: IndexComponent },
	{ path: 'contact/:id', component: DetailComponent},
	{ path: 'add-contact', component: AddComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
