import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AppBootstrapModule } from './shared/app-bootstrap.module';
import { HttpModule } from '@angular/http';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { AddComponent } from './components/add/add.component';
import { EditableComponent } from './components/editable/editable.component';
import { IndexComponent } from './components/index/index.component'
import { DetailComponent } from './components/detail/detail.component';

import { ContactsService } from './services/contacts.service';
import { ContactResolve } from './resolve/contact.resolve';

import { CollapseModule } from 'ngx-bootstrap/collapse';
import { AlertModule } from 'ngx-bootstrap/alert';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { GravatarModule } from 'ng2-gravatar-directive';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
   FooterComponent,
   AddComponent,
   EditableComponent,
   IndexComponent,
   DetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AppBootstrapModule,
    HttpModule,
    CollapseModule.forRoot(),
    AlertModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    GravatarModule
  ],
  providers: [
  	ContactsService,
  	ContactResolve
  	],
  bootstrap: [AppComponent]
})
export class AppModule { }
