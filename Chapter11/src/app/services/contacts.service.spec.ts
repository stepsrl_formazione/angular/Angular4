import { TestBed, inject, async, getTestBed } from '@angular/core/testing';
import { BaseRequestOptions, Http, Response, ResponseOptions, XHRBackend} from '@angular/http';
// ...http imports
import { MockBackend, MockConnection } from '@angular/http/testing';
import { ContactsService } from './contacts.service';
import { Contact } from '../models/contact'; 

// https://semaphoreci.com/community/tutorials/testing-services-in-angular-2
describe('ContactsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ContactsService]
    });
  });

  it('should be created', inject([ContactsService], (service: ContactsService) => {
    expect(service).toBeTruthy();
  }));
});
