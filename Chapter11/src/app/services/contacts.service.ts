import { Injectable } from '@angular/core';
import { SERVER_API_URL } from '../app.constant';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { Contact } from '../models/contact';
import {  ResponseWrapper } from '../models/response-wrapper'
import {  createRequestOption } from './request-util';

@Injectable()
export class ContactsService {

	private resourceUrl = SERVER_API_URL+'contacts';

  constructor(private http: Http) { }
 /* contacts = [
      { id: 11, name: 'Marco' },
      { id: 12, name: 'Fausto' },
      { id: 13, name: 'Nando' },
      { id: 14, name: 'Carmine' },
      { id: 15, name: 'Manuela' },
      { id: 16, name: 'Roberto' },
      { id: 17, name: 'Daniele' },
      { id: 18, name: 'Dalila' },
      { id: 19, name: 'Massimo' },
      { id: 20, name: 'Valentina' }
    ];
*/
  	find(id: number): Observable<Contact> {
  		return this.http.get(this.resourceUrl+'/'+id)
  		.map((res: Response) => {
  			const jsonResponse = res.json();
  			return this.convertItemFromServer(jsonResponse);
  		});
  	}
// multipage version
   /*  query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }
*/
  query(): Observable<ResponseWrapper> {
        return this.http.get(this.resourceUrl)
            .map((res: Response) => this.convertResponse(res));
    }

  	create(contact:Contact): Observable<Contact> {
  		//const copy = this.convert(contact);
  		return this.http.post(this.resourceUrl, contact)
        .map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
  	}

  	 update(contact: Contact): Observable<Contact> {
        const copy = this.convert(contact);

        return this.http.put(this.resourceUrl+"/"+contact.id, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

     delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

  	 /**
     * Convert a returned JSON object to Contact.
     */
    private convertItemFromServer(json: any): Contact {
        const entity: Contact = Object.assign(new Contact(null,null,null,null,null,null,null), json);
    //    entity.time_stamp = this.dateUtils
    //        .convertLocalDateFromServer(json.time_stamp);
        return entity;
    }

     /**
     * Convert a Movie to a JSON which can be sent to the server.
     */
    private convert(contact: Contact): Contact {
        const copy: Contact = Object.assign({}, contact);
       // copy.time_stamp = this.dateUtils
       //     .convertLocalDateToServer(movie.time_stamp);
        return copy;
    }
  

}
