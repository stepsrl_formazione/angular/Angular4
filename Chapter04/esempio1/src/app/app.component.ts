import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
title: string;
description: string;
constructor(){
this.title = 'Angular 4';
this.description = 'Angular 4 component con tag selector.';
}
}
