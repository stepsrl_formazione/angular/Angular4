import { Component, ViewChildren, Input, Output, OnInit, QueryList, EventEmitter } from '@angular/core';

@Component({
  selector: 'div[my-child-comp]',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent  {
private static instanceCount: number = 0;
instanceId: number;
@Input() myText: string;
@Output() onChildMessage = new EventEmitter<string>();
constructor(){
	ChildComponent.instanceCount += 1;
	this.instanceId = ChildComponent.instanceCount;
	this.myText='Child component Default text';
}
onClick(){
	this.onChildMessage.emit(`Hello from ChildComponent with instance id: ${this.instanceId}`);
}

}
